#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define MAX_NAME 60

int main(int argc, char *argv[])
{
    // handling no arguments with helping hint
    if (argc == 1)
    {
        printf("Usage: ./numer YOURNAME.\n");
        return 1;
    }

    char name[MAX_NAME];

    for (int k = 0, arg_l = strlen(argv[1]); k < arg_l; k++)
    {
        name[k] = argv[1][k];
    }


    int count = 0, l_index;

    for (int i = 0; i < strlen(argv[1]); i++)
    {
        char dg = name[i];
        if (isalpha(dg))
        {
            if (islower(dg))
            {
                dg = toupper(dg);
            }

            if (dg == 'A' || dg == 'I' || dg == 'Q' || dg == 'J' || dg == 'Y')
            {
                count += 1;
            }
            else if (dg == 'B' || dg == 'K' || dg == 'R')
            {
                count += 2;
            }
            else if (dg == 'C' || dg == 'G' || dg == 'L' || dg == 'S')
            {
                count += 3;
            }
            else if (dg == 'D' || dg == 'M' || dg == 'T')
            {
                count += 4;
            }
            else if (dg == 'E' || dg == 'H' || dg == 'N' || dg == 'X')
            {
                count += 5;
            }
            else if (dg == 'U' || dg == 'V' || dg == 'W')
            {
                count += 6;
            }
            else if (dg == 'O' || dg == 'Z')
            {
                count += 7;
            }
            else if (dg == 'P' || dg == 'F')
            {
                count += 8;
            }

        }
        else if (isblank(name[i]) || isspace(name[i]) || ispunct(name[i]))
        {
            count += 0;
        }
        else
        {
            printf("Invalid input.\n Hint: Your name contains invaild characters.\n");
            return 1;
        }
    }

    int count_cp = count;
    int count_2 = 0;
    // Bring down to to one digit
    while (count / 10 > 0) {
        count_2 += count % 10;
        count = count / 10;
    }
    count_2 += count;

    // Printing results
    if (count_2 == count_cp) {
        //
        printf("Numberologic number of %s is %i", argv[1], count_cp);
    }
    else {
        printf("Numerologic sum of %s is %i & digit is %i", argv[1], count_cp, count_2);
    }
    printf("\n");

    // success
    return 0;
}