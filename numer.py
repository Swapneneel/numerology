import string
import curses.ascii as ascii

# The lists for having the letter numbers
one = ['A', 'I', 'Q', 'J', 'Y']
two = ['B', 'K', 'R']
three = ['C', 'G', 'L', 'S']
four = ['D', 'M', 'T']
five = ['E', 'H', 'N', 'X']
six = ['U', 'V', 'W']
seven = ['O', 'Z']
eight = ['P', 'F']
# There is no number nine for English Alphabets


# taking input by calling out
name = input("Please Enter the Name: ")

count = 0
# looping over the alphabets in the name
for c in name:
    if ascii.isalpha(c):
        # filtering out the lowercase
        if ascii.islower(c):
            c = c.upper()
        if c in one:
            count += 1
        elif c in two:
            count += 2
        elif c in three:
            count += 3
        elif c in four:
            count += 4
        elif c in five:
            count += 5
        elif c in six:
            count += 6
        elif c in seven:
            count += 7
        elif c in eight:
            count += 8
    elif ascii.isblank(c):
        continue
    else:
        continue



print(f"Numerological count of {name} is {count}\n")